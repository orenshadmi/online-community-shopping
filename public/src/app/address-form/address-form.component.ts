import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomizedValidatorsService } from '../services/customized-validators.service';
import { BackEndService } from '../services/back-end.service';
import { NavbarService } from '../services/navbar.service';



@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})
export class AddressFormComponent implements OnInit {
  paymentForm: FormGroup;
  address : address;
  constructor(private fb: FormBuilder,private CustomizedValidators: CustomizedValidatorsService, private BackEndService: BackEndService,private nav: NavbarService) { 
    this.address = {
      street : '',
      city : '',
      zipcode : 0,
      phone : 0
    }
  }
  @Output() onAddressSubmited =  new EventEmitter<address>();
  isOpen : boolean = true;
  ngOnInit() {
    this.nav.show();
    this.paymentForm = this.fb.group({
      address: [null,[Validators.required]],
      city: ['',[Validators.required]],
      zipCode: [null,[Validators.required]],
      phone: [null,[Validators.required]]
    })
  }
  onSubmit(){
    console.log(this.paymentForm.value,this.BackEndService.cart);
    this.address.street = this.paymentForm.value.address;
    this.address.city = this.paymentForm.value.city;
    this.address.zipcode = this.paymentForm.value.zipCode;
    this.address.phone = this.paymentForm.value.phone;
    this.onAddressSubmited.emit(this.address);
    this.isOpen = false;
    //post action with the address for order element
    
    
    
  }


  get cardNumber(){
    return this.paymentForm.get('cardNumber');
  }
  get cardOwnerName(){
    return this.paymentForm.get('cardOwnerName');
  }
  get exparationDate(){
    return this.paymentForm.get('exparationDate');
  }
  get CVV(){
    return this.paymentForm.get('CVV');
  }

}

interface address {
  street : string,
  city : string,
  zipcode : number,
  phone : number,

}
