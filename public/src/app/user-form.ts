export interface UserForm {
    email: string;
    role: string;
    username: String;
    avatar: String;
}
