import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Product } from '../models/product';

@Component({
  selector: 'app-product-edit-modal',
  templateUrl: './product-edit-modal.component.html',
  styleUrls: ['./product-edit-modal.component.css']
})
export class ProductEditModalComponent implements OnInit {
  @Input() product : Product; 

  constructor(private modalService: NgbModal) { 

  }

  ngOnInit() {
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });

  }

}
