import { TestBed, inject } from '@angular/core/testing';

import { CustomizedValidatorsService } from './customized-validators.service';

describe('CustomizedValidatorsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomizedValidatorsService]
    });
  });

  it('should be created', inject([CustomizedValidatorsService], (service: CustomizedValidatorsService) => {
    expect(service).toBeTruthy();
  }));
});
