import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Action } from '../action';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActionClientService {

  constructor(private http: HttpClient) { }


  invokeAction(action: Action) : Observable<Action>{
    return this.http.post<Action>('http://localhost:8090/smartspace/actions', action);
  
  }
}
