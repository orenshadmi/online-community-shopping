import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { UserForm } from '../user-form';

@Injectable({
  providedIn: 'root'
})
export class UserClientService {
  user: User;
  

  constructor(private http: HttpClient) { 
    
  }


  getAllUserByAdmin(page, size, email, smartspace) : Observable<User[]>{
    return this.http.get<User[]>('http://localhost:8090/smartspace/admin/users/' + smartspace + '/' + email +'?page=' +page+ '&size=' + size);
  
  }

  postUserByAdmin(users: User[], email, smartspace) : Observable<User[]>{
    return this.http.post<User[]>('http://localhost:8090/smartspace/admin/users/' + smartspace + '/' + email, users);
  
  }

  getUserFromDB(email, smartspace) : Observable<User>{
    return this.http.get<User>('http://localhost:8090/smartspace/users/login/' + smartspace + '/' + email);
  
  }

  postUserByUser(user: UserForm[]) {
    fetch('http://localhost:8090/smartspace/users', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json'

      },
      body: JSON.stringify(user)
    })
    .then((res)=> res.json())
    .then((data)=> console.log(data))
  
  }
}
