import { TestBed, inject } from '@angular/core/testing';

import { ElementClientService } from './element-client.service';

describe('ElementClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ElementClientService]
    });
  });

  it('should be created', inject([ElementClientService], (service: ElementClientService) => {
    expect(service).toBeTruthy();
  }));
});
