import { Injectable } from '@angular/core';
import { HttpClientService } from './http-client.service';
import { Product } from '../models/product';
import { ElementClientService } from './element-client.service';
import { UserClientService } from './user-client.service';
import { User } from '../models/user';
import { Cart } from '../models/cart';
import { Key } from '../key';


@Injectable({
  providedIn: 'root'
})
export class BackEndService {
  products: Product[];
  cart: Product[];
  cartId : string;
  users: User[];
  user: User;
  totalPrice: number;
  page= 0;  
  size= 8;
  key : Key;
  
  constructor(private elementService : ElementClientService,private userService : UserClientService) {
    this.products =[];
    this.cart=[];
    this.users=[];
    this.user =null;
    this.totalPrice=0;
    // this.getProductsFromDB();
  }
  getProductsFromDB(){ 
    
    // this.elementService.getProductElementsBySearch(this.page,this.size,"a","a")
    //   .subscribe(data => {
    //     JSON.stringify(data)
    //     this.setProducts(data)
    //   })    
  }

  setProducts(data){
    
    for(let i=0;i<data.length ;i++){
      
      let productToPush = new Product(
        data[i].name,
        
        parseFloat(data[i].elementProperties.price),
        data[i].elementProperties.src, 
        localStorage.getItem('email'),
        localStorage.getItem('smartspace'),
        data[i].elementProperties.description,
        data[i].elementProperties.size,
        data[i].elementProperties.color,
        data[i].key.id,
        data[i].key.smartspace,
        data[i].created,
        data[i].elementProperties.totalLikes
        )
        // productToPush.key = (data[i].key);
        
      
      this.products.push(productToPush);

    }
    
  }

  postData(url = ``, data = {}){
    return fetch(url, {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
          "Content-Type": "application/json",
      },
      redirect: "follow",
      referrer: "no-referrer",
      body: JSON.stringify(data),
    })
    .then(response => response.json()); // parses JSON response into native Javascript objects 
  }

  insertProductToCart(product:Product){
    this.cart.push(product);
  }
  removeProductFromCart(product:Product){
    const index = this.cart.indexOf(product);
    this.cart.splice(index, 1);
  }
  addToTotalPrice(price:number){
    this.totalPrice+=price;
  }
  removeFromTotalPrice(price:number){
    this.totalPrice-=price;
  }
}