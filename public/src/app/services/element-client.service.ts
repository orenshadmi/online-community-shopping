import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { IElement } from '../models/element';
import { Cart } from '../models/cart';

@Injectable({
  providedIn: 'root'
})
export class ElementClientService {

  constructor(private http: HttpClient) { }
  
  postCartElementByManager(cart : Cart) : Observable<any>{
    return this.http.post<Cart>('http://localhost:8090/smartspace/elements/2019BTal.Cohen/AlonSamay@gmail.com', cart);
  }

  putProductElementByManager(product : Product, email : string, smartspace : string) : Observable<any>{
    console.log(product);
    return this.http.put<Product>('http://localhost:8090/smartspace/elements/' + smartspace + '/' + email + '/2019BTal.Cohen/'.concat(product.key.id), product);
  }

  postProductElementByManager(product : Product, email, smartspace ) : Observable<any>{
    return this.http.post<Product>('http://localhost:8090/smartspace/elements/' + smartspace + '/' + email, product);
  }

  getProductElementsBySearch(page,size,smartspace,email, value): Observable<Product[]>{
    return this.http.get<Product[]>('http://localhost:8090/smartspace/elements/' + smartspace + '/' + email +'?search=type&value='+value +'&page=' + page + '&size=' + size);
  }

  getElementByUser(): Observable<IElement>{

    return this.http.get<IElement>('http://localhost:8090/smartspace/elements/userSmartSpace/alon@gmail.com/my_smartspace/5ccad068a28af10502b9b2a1');
    
  }

  


}
