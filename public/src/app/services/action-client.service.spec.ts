import { TestBed, inject } from '@angular/core/testing';

import { ActionClientService } from './action-client.service';

describe('ActionServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActionClientService]
    });
  });

  it('should be created', inject([ActionClientService], (service: ActionClientService) => {
    expect(service).toBeTruthy();
  }));
});
