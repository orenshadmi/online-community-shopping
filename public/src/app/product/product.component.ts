import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Product } from '../models/product';
import { BackEndService } from '../services/back-end.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
import { ElementClientService } from '../services/element-client.service';
import { UserClientService } from '../services/user-client.service';
import { NavbarService } from '../services/navbar.service';
import { AuthService } from '../auth.service';
import { ActionClientService } from '../services/action-client.service';
import { Action } from '../action';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  private eventsSubject: Subject<void> = new Subject<void>();
  
  products: Product[];
  @Input() toShowAddProductCard;
  @Input() toShowProductsFromDb;
  @ViewChild('likeImg') el:ElementRef;
  

  ngOnInit() {
    this.nav.show();

  }


  @Input() product:Product;
  constructor(private actionService: ActionClientService, private authService : AuthService, public nav: NavbarService,private userService : UserClientService, private elementService : ElementClientService ,private backEndService: BackEndService , private modalService: NgbModal, private fb: FormBuilder) {
  }
  addOneItem(){
    let amount = this.product.elementProperties.amount;
    if(amount<100){
      this.backEndService.addToTotalPrice(this.product.elementProperties.price);
      if(amount===0){
        this.backEndService.insertProductToCart(this.product);
      }
      this.product.elementProperties.amount ++;
    }
  }
  removeOneItem(){
    let amount = this.product.elementProperties.amount;
    if(amount>0){
      this.backEndService.totalPrice-=this.product.elementProperties.price;
      if(amount===1){
        this.backEndService.removeProductFromCart(this.product);
      }
      this.product.elementProperties.amount --;
    }
  }
  // openLg(content) {
  //   this.modalService.open(content, { size: 'lg' });
  // }

  emitEventToChild() {
    this.eventsSubject.next()
  }


  
  
  like(){
    // to pass product key instead of the null
    this.actionService.invokeAction(new Action("like",localStorage.getItem('smartspace'), localStorage.getItem('email'),this.product.key.id))
    .subscribe(data => {
      console.log(data)
      this.product.elementProperties.totalLikes++;
      
    });

  }

}