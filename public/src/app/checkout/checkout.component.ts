import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ElementClientService } from '../services/element-client.service';
import { Cart } from '../models/cart';
import { BackEndService } from '../services/back-end.service';

import { ActionClientService } from '../services/action-client.service';
import { Action } from '../action';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  showAddress = true;
  showPayment = false;
  showSummary = false;
  // shippingInfo: Array<any>;
  paymentInfo: Array<string>;
  address: address;
  geolocationPosition: any;
  // address: string;
  taxRate: number = 0.17;
  deliveryPrice: number = 30;
  @ViewChild('someVar') el: ElementRef;
  @ViewChild('summary') summary: ElementRef;
  @ViewChild('placeOrderButton') placeOrderButton: ElementRef;



  constructor(private router: Router, private actionService: ActionClientService, private elementService: ElementClientService, private backEndService: BackEndService) {
    this.address = {
      street: '',
      city: '',
      zipcode: 0,
      phone: 0
    }
  }

  ngOnInit() {
  }


  placeOrder() {
    setTimeout(() => {
      this.el.nativeElement.style.display = 'none';
      this.postActionAndNavigateHome();
    }, 3000);

    this.placeOrderButton.nativeElement.style.display = 'none';
    this.summary.nativeElement.style.display = 'none';
    this.el.nativeElement.style.display = 'block';
  }

  handlePaymentSubmited(e) {
    console.log(e);
    this.paymentInfo = e;
    this.showPayment = false;
    this.showSummary = true;
  }

  handleAddressSubmited(e) {

    this.address = e;
    console.log(this.address);
    this.showAddress = false;
    this.showPayment = true;

  }

  postActionAndNavigateHome() {
    console.log(this.backEndService.cart);
    console.log(this.backEndService.totalPrice);
    this.actionService.invokeAction(new Action("CheckOut", undefined, localStorage.getItem('email'), localStorage.getItem('cartId')))
      .subscribe(data => {
        console.log(data);

        this.backEndService.cart = [];
        this.backEndService.totalPrice = 0;
        this.createCartAction();


      });
    //send checkout to server
    //set current cart to expired and add it to user history
    //create new cart for user
    // this.elementService.postCartElementByManager(new Cart())
    // .subscribe(data => console.log(data));
  }

  createCartAction() {
    this.actionService.invokeAction(new Action("CreateCart", localStorage.getItem('smartspace'), localStorage.getItem('email'), '5', undefined, undefined))
      .subscribe(data => {
        localStorage.setItem('cartId',data.properties.cartKey.id);
        this.getLocationOfUser();

      });
  }

  getLocationOfUser() {
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.geolocationPosition = position,


            this.checkIn(position);



        },
        error => {
          switch (error.code) {
            case 1:
              console.log('Permission Denied');
              break;
            case 2:
              console.log('Position Unavailable');
              break;
            case 3:
              console.log('Timeout');
              break;
          }
        }
      );
    };
  }

  checkIn(position) {
    let latlng = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    }
    this.actionService.invokeAction(new Action("CheckIn", undefined, localStorage.getItem('email'), localStorage.getItem('cartId'), undefined, undefined, latlng))
      .subscribe(data => {
        console.log(data);
        this.backEndService.cart = []
        this.backEndService.totalPrice = 0;
        this.router.navigate(['/home']);


      });
    }


}

interface address {
  street: string,
  city: string,
  zipcode: number,
  phone: number,

}
