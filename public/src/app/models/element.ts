type key = {
    id: string;
    smartspace : string;
}

type latlng = {
    lat: String;
    lng: String;
};

type creator = {
    email: string;
    smartspace: string;
}

type elementProperties = {
        att1: String;
        att2: String;
        att3: String;
}


export interface IElement {
        key : key;
        elementType: String;
        name: String;
        expired: boolean;
        created: Date;
        creator: creator;
        latlng: latlng;
        elementProperties : elementProperties
}


