import { Product } from "./product";


type key = {
    elementId: string;
    smartspace : string;
}

type latlng = {
    lat: String;
    lng: String;
};

type creator = {
    email: string;
    smartspace: string;
}

type elementProperties = {
        products : Product[];
        totalItems: number;
        totalCost: number;
        userEmail : string,
        userSmartspace: string
}

type userKey = {
    userEmail : string;
    userSmartspace: string;
}

export class Cart {
    key : key;
    elementType: String;
    name: string;
    expired: boolean;
    created: Date;
    creator: creator;
    latlng: latlng;
    elementProperties : elementProperties;
    product: Product[];


    
    // priceOfOneProduct: number;
    // src: string;
    // amount: number;
    // isInCart: boolean;
    // totalPrice: number;
    constructor(email : string ,smartspace: string){
        this.name="user-cart";
        this.elementType = "cart";
        this.expired = false;
        this.creator = {
            email: "manager@email.com",
            smartspace:"2019BTal.Cohen"
        }
        this.latlng = {
            lat : "1.0",
            lng : "2.0"
        }
        this.elementProperties= {
            products : null,        
            totalCost : 0,
            totalItems : 0,
            userEmail : email,
            userSmartspace: smartspace
            
        }

        
        

        
        

        // this.priceOfOneProduct=price;
        // this.src=src;
        // this.amount=0;
        // this.isInCart = false;
        // this.totalPrice=0;
    }
}