

type key = {
    id: string;
    smartspace : string;
}

type latlng = {
    lat: number;
    lng: number;
};

type creator = {
    email: string;
    smartspace: string;
}

type elementProperties = {
        src : string;
        price: number;
        amount : number;
        description?: string;
        size?: string;
        color?: string;
        totalLikes? : number;
}

export class Product {
    key ?: key;
    elementType: String;
    name: string;
    expired: boolean;
    created: Date;
    creator: creator;
    latlng: latlng;
    elementProperties : elementProperties


    
    priceOfOneProduct: number;
    src: string;
    amount: number;
    // isInCart: boolean;
    // totalPrice: number;
    constructor(name, price : number, src, creatorEmail, creatorSmartspace,description? : string,size?: string,color? :string,id? :string, smartspace? : string,created? : Date,totalLikes? : number){
        this.name=name;
        this.elementType = "product";
        this.expired = false;

        this.key = {
            id : id,
            smartspace : smartspace
        }

        this.created = created;
        
        this.creator = {
            email: creatorEmail,
            smartspace : creatorSmartspace
        };
        this.latlng = {
            lat: 2.0,
            lng: 3.0
        };
        this.elementProperties = {
            src : src,
            price : price,
            description : description,
            size : size,
            color : color,
            totalLikes: totalLikes,
            amount : 0,
            
        }
        
        // this.isInCart = false;
        // this.totalPrice=0;
    }

}