type key = {

}

export class User {
    key :{
        email: string ;
        smartspace: string;
    };
    role: string;
    username: String;
    avatar: String;
    points: number;
}

