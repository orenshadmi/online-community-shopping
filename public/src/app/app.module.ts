import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { UsersComponent } from './users/users.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TableComponent } from './table/table.component';
import { LoginComponent } from './login/login.component';
import { NavbarService } from './services/navbar.service';
import { RegisterComponent } from './register/register.component';
import { ProductComponent } from './product/product.component';
import { MapComponent } from './map/map.component';
import {AgmCoreModule} from '@agm/core';
import {ReactiveFormsModule} from '@angular/forms'
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AdminComponent } from './admin/admin.component';
import { AddUserComponent } from './add-user/add-user.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CartProductComponent } from './cart-product/cart-product.component';
import { PaymentFormComponent } from './payment-form/payment-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material';
import {  MatNativeDateModule,  MatFormFieldModule } from '@angular/material';
import { ModalComponent } from './modal/modal.component';
import { ProductInfoModalComponent } from './product-info-modal/product-info-modal.component';
import { ProductEditModalComponent } from './product-edit-modal/product-edit-modal.component';
import { EditProductFormComponent } from './edit-product-form/edit-product-form.component';
import { MapFormComponent } from './map-form/map-form.component';
import { LocationComponent } from './location/location.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AddressFormComponent } from './address-form/address-form.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';
import { AddProductCardComponent } from './add-product-card/add-product-card.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UsersComponent,
    HomeComponent,
    NotFoundComponent,
    TableComponent,
    LoginComponent,
    RegisterComponent,
    ProductComponent,
    MapComponent,
    AdminComponent,
    AddUserComponent,
    ShoppingCartComponent,
    CartProductComponent,
    PaymentFormComponent,
    ModalComponent,
    ProductInfoModalComponent,
    ProductEditModalComponent,
    EditProductFormComponent,
    MapFormComponent,
    LocationComponent,
    CheckoutComponent,
    AddressFormComponent,
    OrderSummaryComponent,
    AddProductCardComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: ' AIzaSyAFZ_zIndxLu7a2PCzCVQEPV6Rva929F9Q'
    }),
    RouterModule.forRoot([
      {
        path: '', 
        redirectTo: '/login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'location',
        component: LocationComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      }, 
      {
        path: 'admin',
        component: AdminComponent
      }, 
      {
        path: 'home/checkout',
        component: CheckoutComponent
      },
      {
        path: '**',
        component: NotFoundComponent
      },   
    ])],
  
  providers: [NavbarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
