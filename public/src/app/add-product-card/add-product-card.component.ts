import { Component, OnInit } from '@angular/core';
import { ActionClientService } from '../services/action-client.service';
import { AuthService } from '../auth.service';
import { NavbarService } from '../services/navbar.service';
import { UserClientService } from '../services/user-client.service';
import { ElementClientService } from '../services/element-client.service';
import { BackEndService } from '../services/back-end.service';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { Product } from '../models/product';

@Component({
  selector: 'app-add-product-card',
  templateUrl: './add-product-card.component.html',
  styleUrls: ['./add-product-card.component.css']
})
export class AddProductCardComponent implements OnInit {
  productForm: FormGroup;
  constructor(private actionService: ActionClientService, private authService : AuthService, public nav: NavbarService,private userService : UserClientService, private elementService : ElementClientService ,private backEndService: BackEndService , private fb: FormBuilder) { }

  ngOnInit() {
    this.productForm = this.fb.group({
      name: [null,[Validators.required]], 
      price: [null,[Validators.required]],
      src: [null,[Validators.required]],
      description: [null,[Validators.required]],
      size: [null,[Validators.required]],
      color: [null,[Validators.required]],
    })
  }


  saveProduct(formDirective : FormGroupDirective){
    let proudctToSave =new Product(
      this.productForm.value.name ,
      this.productForm.value.price,
      this.productForm.value.src , 
      localStorage.getItem('email'),
      localStorage.getItem('smartspace'),
      this.productForm.value.description ,
      this.productForm.value.size , 
      this.productForm.value.color,
      undefined,
      undefined,
      new Date(),
      0 
      );
      
      // this.products.push(proudctToSave);

      console.log(proudctToSave) 

    this.elementService.postProductElementByManager(proudctToSave,localStorage.getItem('email'), localStorage.getItem('smartspace'))
        .subscribe(data=>{
          console.log(data);
          this.backEndService.setProducts(data);
          formDirective.resetForm();
          this.productForm.reset();
        });  
  }

}
