import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomizedValidatorsService } from '../services/customized-validators.service';
import { BackEndService } from '../services/back-end.service';
import { NavbarService } from '../services/navbar.service';
import { ActionClientService } from '../services/action-client.service';
import { Action } from '../action';


@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.css']
})
export class PaymentFormComponent implements OnInit {
  paymentForm: FormGroup;
  @Input() address : address;
  paymentInfo : payment;
  isOpen : boolean = false;
  

  constructor(private actionService : ActionClientService, private fb: FormBuilder,private CustomizedValidators: CustomizedValidatorsService, private backEndService: BackEndService,private nav: NavbarService) { 
    this.paymentInfo = {
      cardNumber: 0,
      cardOwnerName: '',
      exparationDate: null,
      CVV: 0
    }
  }
  @Output() onPaymentSubmited =  new EventEmitter<string[]>();

  ngOnInit() {
    this.nav.show();
    this.paymentForm = this.fb.group({
      cardNumber: ['',[
        Validators.required,
        this.CustomizedValidators.validateLength,
        this.CustomizedValidators.areAllNumbers,
        this.CustomizedValidators.validateSum,
        this.CustomizedValidators.areAllDigitsTheSame,
        this.CustomizedValidators.isLastDigitEven
      ]],
      cardOwnerName: ['',[Validators.required]],
      exparationDate: [null,[Validators.required]],
      CVV: [null,[Validators.required]]
    })
    this.isOpen = true;
    console.log(this.address);
  }
  onSubmit(){
    console.log(this.paymentForm.value,this.backEndService.cart);
    this.paymentInfo.cardNumber = this.paymentForm.value.cardNumber;
    this.paymentInfo.cardOwnerName = this.paymentForm.value.cardOwnerName;
    this.paymentInfo.exparationDate = this.paymentForm.value.exparationDate;
    this.paymentInfo.CVV = this.paymentForm.value.CVV;


    this.onPaymentSubmited.emit(this.paymentForm.value);
    this.actionService.invokeAction(new Action("Pay", localStorage.getItem('smartspace'), localStorage.getItem('email'), localStorage.getItem('cartId'),undefined,this.paymentInfo,undefined,undefined,this.address))
    .subscribe(data => console.log(data));
    //post action with payment details to order.
    
  }


  get cardNumber(){
    return this.paymentForm.get('cardNumber');
  }
  get cardOwnerName(){
    return this.paymentForm.get('cardOwnerName');
  }
  get exparationDate(){
    return this.paymentForm.get('exparationDate');
  }
  get CVV(){
    return this.paymentForm.get('CVV');
  }

}

interface address {
  street : string,
  city : string,
  zipcode : number,
  phone : number,

}

interface payment {
  cardNumber: number;
  cardOwnerName: string;
  exparationDate: Date;
  CVV: number;

}