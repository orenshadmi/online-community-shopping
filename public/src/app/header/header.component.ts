import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { ActionClientService } from '../services/action-client.service';

import { BackEndService } from '../services/back-end.service';
import { Action } from '../action';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private actionService : ActionClientService ,private backEndService  : BackEndService ,  public authService: AuthService, public nav: NavbarService,private router : Router) { }

  ngOnInit() {
  }

  onLogOut(){
    //CHANGE TO CHECKOUT
    this.actionService.invokeAction(new Action("CheckOut", undefined, localStorage.getItem('email'),localStorage.getItem('cartId')))
    .subscribe(data => {
      console.log(data);

      localStorage.clear();
      this.router.navigate(['/login']);  
      
    });
    
  }

}
