import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { UserClientService } from '../services/user-client.service';
import { BackEndService } from '../services/back-end.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  myForm : FormGroup;
  // users: User[];
  
  constructor(private backEndService: BackEndService,private fb: FormBuilder, private userService: UserClientService,private router:Router) { }
  @Output() modalClose =  new EventEmitter<string>();
  

  ngOnInit() {
    this.myForm = this.fb.group({ 
      key: this.fb.group({
        email: '',
        smartspace: ''
      }),
      username: '',
      avatar: '',
      role: ''
    })
    
  }

 

 

   onCloseModal(event: any){
    console.log(this.myForm.value);
    const arr: Array<User> = [this.myForm.value];
    this.userService.postUserByAdmin(arr, localStorage.getItem('email'), localStorage.getItem('smartspace'))
      .subscribe(data => {
        
        this.backEndService.users.push(data[0]);
        this.modalClose.emit(event.target.value); 
      });

        
  }

}
