import { Component, OnInit } from '@angular/core';
import { BackEndService } from '../services/back-end.service';
import { Product } from '../models/product';
import { HttpClientService } from '../services/http-client.service';
import { NavbarService } from '../services/navbar.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../models/user';
import { AuthService } from '../auth.service';
import { UserClientService } from '../services/user-client.service';

import { ActionClientService } from '../services/action-client.service';
import { Action } from '../action';
import { ElementClientService } from '../services/element-client.service';


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})

export class ShoppingCartComponent implements OnInit {


  cart: Product[];
  user: User;
  products: string[]
  page: number = 0;
  size: number;
  disableNext = false;

  constructor(private backendService: BackEndService, private elementService: ElementClientService, private actionService: ActionClientService, private authService: AuthService, private BackEndService: BackEndService, public nav: NavbarService, private httpService: HttpClientService, private modalService: NgbModal) {
    this.cart = this.BackEndService.cart;
    this.products = [];
    this.setSize();

  }
  
  setSize(){
    if(this.authService.isUserManager())
      this.size = 7;
    else{
      this.size = 9;
    }
  }

  ngOnInit(): void {
    this.getNextPageUsers()
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  onProceedToCheckout() {

    this.cart.forEach(element => {

      this.products.push(element.key.id);
    });


    this.actionService.invokeAction(new Action("UpdateCart", localStorage.getItem('smartspace'), localStorage.getItem('email'), localStorage.getItem('cartId'), this.products, undefined, undefined))
      .subscribe(data => console.log(data));
  }



  nextPage() {
    console.log("next pressed");
    console.log(this.page);
    //get current users
    this.elementService.getProductElementsBySearch(++this.page, this.size, localStorage.getItem('smartspace'), localStorage.getItem('email'), 'product')
      .subscribe(data => {
        this.backendService.products = [];
        this.backendService.setProducts(data);
        //get next pages users
        this.getNextPageUsers();
      });
  }

  previousPage() {
    console.log("prev pressed");

    this.elementService.getProductElementsBySearch(--this.page, this.size, localStorage.getItem('smartspace'), localStorage.getItem('email'), 'product')
      .subscribe(data => {
        if (this.page < 0) {
          this.page = 0;
        }
        JSON.stringify(data)
        console.log(data);
        this.backendService.products = [];
        this.backendService.setProducts(data);
        if (this.page < 0) {
          this.page = 0;
        }

        this.disableNext = false;


      });
  }

  getNextPageUsers() {
    this.elementService.getProductElementsBySearch(++this.page, this.size, localStorage.getItem('smartspace'), localStorage.getItem('email'), 'product')
      .subscribe(data => {
        this.page--;
        if (data.length == 0) {
          this.disableNext = true;
        }

      });
  }
}