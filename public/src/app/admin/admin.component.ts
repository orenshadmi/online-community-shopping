import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { User } from '../models/user';
import { UserClientService } from '../services/user-client.service';
import { BackEndService } from '../services/back-end.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public users: User[] = [];
  page: number;
  size: number = 10;
  disableNext = false;

  constructor(private backEndService: BackEndService, public nav: NavbarService, private userService: UserClientService) {
    this.page = 0;
  }


  ngOnInit() {
    this.nav.show();
    this.getUsers();

  }

  getUsers(){
    this.userService.getAllUserByAdmin(this.page, this.size, localStorage.getItem('email'), localStorage.getItem('smartspace'))
      .subscribe(data => {
        this.backEndService.users = data;
        this.getNextPageUsers();
      });
  }

  getNextPageUsers(){
    this.userService.getAllUserByAdmin(++this.page, this.size, localStorage.getItem('email'), localStorage.getItem('smartspace'))
    .subscribe(data => {
      this.page--;
      if (data.length == 0) {       
        this.disableNext = true;
      }

    });
    
  }



  nextPage() {
    console.log("next pressed");
    console.log(this.page);
    //get current users
    this.userService.getAllUserByAdmin(++this.page, this.size, localStorage.getItem('email'), localStorage.getItem('smartspace'))
      .subscribe(data => {
        console.log(data);
        this.backEndService.users = data;
        //get next pages users
        this.getNextPageUsers();
      });
  }



  previousPage() {
    console.log("prev pressed");

    this.userService.getAllUserByAdmin(--this.page, this.size, localStorage.getItem('email'), localStorage.getItem('smartspace'))
      .subscribe(data => {
        if (this.page < 0) {
          this.page = 0;
        }
        console.log(this.page);
        console.log(data);
        this.backEndService.users = data;
        this.disableNext = false;

      });
  }
}
