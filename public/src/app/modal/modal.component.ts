import { Output, EventEmitter, Component } from "@angular/core";
import { Router } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';



// app-modal.component.ts
@Component({
  selector: 'app-modal',
  exportAs: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  

  constructor(private modalService: NgbModal, private router : Router ) {}
    
  

  addUser(content){
    this.modalService.open(content, { size: 'lg', centered: true });
  }
}