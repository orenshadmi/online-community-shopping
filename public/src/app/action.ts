import { Product } from "./models/product";
import { Optional } from "@angular/core";
import { Latlng } from "./latlng";

type actionKey = {
    smartspace: string;
    id: string;
}

type elementKey = {
    id: string;
    smartspace : string;
}

type cartKey = {
    id: string;
    smartspace : string;
}

type userKey = {
    email: string;
    smartspace : string;
}

type properties = {
    products : string[];
    location :location ;
    paymentInfo :paymentInfo;
    cartKey : cartKey;
    address? :address;
}   

type paymentInfo = {
    cardNumber: number;
    cardOwnerName: string;
    exparationDate: Date;
    CVV: number;
}

type address = {
    street: string;
    city: string;
    zipcode : number;
    phone: number;
}

export type location = {
    lat : number;
    lng : number;
}




export class Action {
    actionKey :actionKey;
    type: string ;
    
    element : elementKey;
    player : userKey;
    properties : properties;
    

        constructor(type,  elementSmartSpace, email, elementId? : string ,products? : string[], paymentInfo?: paymentInfo, latlng?: Latlng, cartKey?: cartKey, address?:address){
        this.type = type;
        // this.actionKey = {
        //     actionSmartSpace : smartspace,
        //     actionId : actionId
            
        // };
        this.element = {
            id : elementId,
            smartspace : '2019BTal.Cohen'
        };
        this.player = {
            email : email,
            smartspace : '2019BTal.Cohen'
        };
        this.properties = {
            cartKey : cartKey,
            products : products,
            paymentInfo : paymentInfo,
            location : latlng,
            address  : address
            
        };
    }
}
