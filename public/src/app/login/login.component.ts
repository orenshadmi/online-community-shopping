import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { Router } from '@angular/router';
import { ElementClientService } from '../services/element-client.service';

import { UserClientService } from '../services/user-client.service';
import { BackEndService } from '../services/back-end.service';
import { ActionClientService } from '../services/action-client.service';
import { Action } from '../action';


type latlng = {
  lat: number;
  lng: number;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  smartspace: string;
  geolocationPosition: any;
  @ViewChild('someVar') el:ElementRef;


  constructor(private actionService: ActionClientService, private userService: UserClientService, public nav: NavbarService, private router: Router, private elementService: ElementClientService, private backEndService: BackEndService) {

  }

  ngOnInit() {
    this.nav.hide();
    localStorage.clear();
    console.log(this.smartspace);
    console.log(this.email);
  }

  showAlert(){
    setTimeout(()=>{
      this.el.nativeElement.style.display = 'none';
  }, 3000);


    this.el.nativeElement.style.display = 'block';
  }


  onLogin() {
    this.userService.getUserFromDB(this.email, this.smartspace)
      .subscribe(data => {

        this.userService.user = data;
        localStorage.setItem('email', this.userService.user.key.email);
        localStorage.setItem('smartspace', this.userService.user.key.smartspace);
        localStorage.setItem('role', this.userService.user.role);
        console.log(this.userService.user);
        this.createCartAction();
      },
      err => {
        this.showAlert();
      });
      
      

  }
  createCartAction() {
    this.actionService.invokeAction(new Action("CreateCart", localStorage.getItem('smartspace'), localStorage.getItem('email'), '21', undefined, undefined))
      .subscribe(data => {
        // this.backEndService.cartId = data.properties.cartKey.id;
        localStorage.setItem('cartId',data.properties.cartKey.id);
        this.getLocationOfUser();

      });
  }


  getLocationOfUser() {
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.geolocationPosition = position,


            this.checkIn(position);



        },
        error => {
          switch (error.code) {
            case 1:
              console.log('Permission Denied');
              break;
            case 2:
              console.log('Position Unavailable');
              break;
            case 3:
              console.log('Timeout');
              break;
          }
        }
      );
    };
  }

  checkIn(position) {

    let latlng = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    }
    this.actionService.invokeAction(new Action("CheckIn", undefined, localStorage.getItem('email'), localStorage.getItem('cartId'), undefined, undefined, latlng))
      .subscribe(data => {
        console.log(data);
        this.backEndService.cart = []
        this.backEndService.totalPrice = 0;
        this.router.navigate(['/home']);


      });
  }
}




