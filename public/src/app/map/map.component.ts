import { Component, OnInit, Input } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { NavbarService } from '../services/navbar.service';
import { ElementClientService } from '../services/element-client.service';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  page: number = 0;
  size: number = 100;
  cartsThatAreOnline = [];
  markers: marker[] = [];
  @Input() distance : number;


  constructor(public nav: NavbarService, private elementService: ElementClientService) { }

  ngOnInit() {
    this.nav.show();
    this.elementService.getProductElementsBySearch(this.page, this.size, localStorage.getItem('smartspace'), localStorage.getItem('email'), 'cart')
      .subscribe(data => {
        JSON.stringify(data)
        console.log(data);
        data.forEach(element => {
          if (!element.expired) {
            let marker: marker =
            {
              lat: element.latlng.lat,
              lng: element.latlng.lng,
              label: 'A',
              draggable: false,
              animation: 'DROP',
              name : element.name
            }
            this.markers.push(marker);
          }

        })
      });
      
  }



  // google maps zoom level
  zoom: number = 18;

  // initial center position for the map
  lat: number = 32.113165;
  lng: number = 34.818044;


  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true,
      animation: 'DROP'

    });
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markerOver(m: marker) {
    m.animation = 'BOUNCE';
  }

  markerOut(m: marker) {
    m.animation = '';
  }
}



// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  animation: 'DROP' | 'BOUNCE' | '';
  name? : string;
}







