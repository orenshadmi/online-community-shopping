import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomizedValidatorsService } from '../services/customized-validators.service';
import { BackEndService } from '../services/back-end.service';
import { NavbarService } from '../services/navbar.service';
import { Product } from '../models/product';
import { HttpClientService } from '../services/http-client.service';
import { ElementClientService } from '../services/element-client.service';


@Component({
  selector: 'app-edit-product-form',
  templateUrl: './edit-product-form.component.html',
  styleUrls: ['./edit-product-form.component.css']
})
export class EditProductFormComponent implements OnInit {

  @Input() product : Product;

  @Output() modalClose =  new EventEmitter<string>();

  productForm: FormGroup;
  constructor(private elementService: ElementClientService, private fb: FormBuilder,private CustomizedValidators: CustomizedValidatorsService, private BackEndService: BackEndService,private nav: NavbarService) { }

  ngOnInit() {
    
    this.nav.show();
    this.productForm = this.fb.group({
      name: '', 
      price: 0.0,
      src: '',
      size: '',
      color: '',
      description: ''
    })

    console.log(this.product);
  }
  onSubmit(event: any){
      
      let proudctToSave =new Product(
        this.productForm.value.name ,
        this.productForm.value.price,
        this.productForm.value.src , 
        localStorage.getItem('email'),
        localStorage.getItem('smartspace'),
        this.productForm.value.description ,
        this.productForm.value.size , 
        this.productForm.value.color,
        this.product.key.id,
        this.product.key.smartspace,
        this.product.created,
        this.product.elementProperties.totalLikes
        );    
      // this.products.push(proudctToSave);

    console.log(proudctToSave);

    
    this.elementService.putProductElementByManager(proudctToSave,localStorage.getItem('email'), localStorage.getItem('smartspace'))
        .subscribe(data=>{
          console.log(data);        
        });

    this.modalClose.emit(event.target.value);           
    
    // console.log(this.paymentForm.value,this.BackEndService.cart);
    // this.BackEndService.postData(`http://localhost:3000/Order`, [this.paymentForm.value,this.BackEndService.cart])
    // .then(data => console.log(JSON.stringify(data)))
    // .catch(error => console.error(error));
  }




}
