import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }


  getCurrentUserEmail(){
    return localStorage.getItem('email');
  }

  getCurrentUserSmartSpace(){
    return localStorage.getItem('smartspace');
  }

  getCurrentRole(){
    return localStorage.getItem('role');
  }

  isUserAdmin(){
    return localStorage.getItem('role') == "ADMIN";
  }

  isUserManager(){
    return localStorage.getItem('role') == "MANAGER";
  }

  isUserPlayer(){
    return localStorage.getItem('role') == "PLAYER";
  }
}
