import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-map-form',
  templateUrl: './map-form.component.html',
  styleUrls: ['./map-form.component.css']
})
export class MapFormComponent implements OnInit {
  mapForm: FormGroup;
  distance:number;

@Output() onSubmitDistance = new EventEmitter<number>();

  constructor(private fb: FormBuilder) { 
    this.distance = 0;
  }

  isDistanceNull() : boolean{
      return this.distance == 0;
  }
  

  ngOnInit() {
    // Get all elements that online and mark them on the map.
    this.mapForm = this.fb.group({
      distance:0
    })
  }

  onSubmit(){
    this.onSubmitDistance.emit(this.mapForm.value.distance)
    //get all elements that are in the distance from the user location and mark them on the map
  }

}
