import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms'
import { User } from '../models/user';
import { Router } from '@angular/router';
import { UserClientService } from '../services/user-client.service';
import { UserForm } from '../user-form';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  myForm : FormGroup;
  

  constructor(private fb: FormBuilder, private userService: UserClientService, private router: Router) { }



  ngOnInit() {
    this.myForm = this.fb.group({
      email: '',
      username: '',
      avatar: '',
      role: ''
    })
  }

  onSubmit(){
    
    console.log(this.myForm.value);
    
    
    
    this.userService.postUserByUser(this.myForm.value)
    this.router.navigate(['/login']);

  }

}
