import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {
  distance : number;

  constructor() { 
    this.distance =0;
  }

  ngOnInit() {
  }

  handleSubmitDistance(event){
    this.distance = event;
    console.log(event)
    
  }

}
