import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { HttpClientService } from '../services/http-client.service';
import { ElementSchemaRegistry } from '@angular/compiler';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ElementClientService } from '../services/element-client.service';
import { BackEndService } from '../services/back-end.service';
import { UserClientService } from '../services/user-client.service';
import { Product } from '../models/product';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit ,OnDestroy{
  ngOnDestroy(): void {
    // console.log(this.backendService.products);
    this.backendService.products = [];
  }

  page = 0;
  size : number;
  constructor(private authService: AuthService,public nav: NavbarService,public backendService: BackEndService,private userService: UserClientService, private elementService: ElementClientService, private modalService: NgbModal) { 
    this.setSize();
  }

  ngOnInit() {
    this.nav.show();
    this.getProductsFromDb();

  }

  setSize(){
    if(this.authService.isUserManager())
      this.size = 7;
    else{
      this.size = 9;
    }
  }




  getProductsFromDb(){
    this.elementService.getProductElementsBySearch(this.page,this.size,localStorage.getItem('smartspace'),localStorage.getItem('email'),'product')
    .subscribe(data => {
      JSON.stringify(data)
      console.log(data);
      this.backendService.setProducts(data)
    });
  }
  

     
  



}
