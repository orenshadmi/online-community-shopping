package IntegrationTests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import smartspace.Application;
import smartspace.dao.EnhancedElementDao;
import smartspace.dao.EnhancedUserDao;
import smartspace.data.*;
import smartspace.data.util.EntityFactory;
import smartspace.layout.ElementBoundary;
import smartspace.layout.UserBoundary;
import smartspace.logic.ElementServiceImp;

import javax.annotation.PostConstruct;
import javax.lang.model.element.Element;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"spring.profiles.active=default"})
public class ElementIntegrationTests {

    private static final String ADMIN_URL = "/admin/elements/{adminSmartspace}/{adminEmail}";
    private static final String USER_URL = "/elements/{userSmartspace}/{userEmail}";
    private static final String MANAGER_URL = "/elements/{managerSmartspace}/{managerEmail}";
    private static final String ELEMENTS_URL = "/{elementSmartspace}/{elementId}";

    private int port;
    private String baseUrl;

    private EntityFactory factory;
    private EnhancedUserDao<UserKey> userDao;
    private EnhancedElementDao<ElementKey> elementDao;
    private RestTemplate restTemplate;

    private static int counter = 0;
    private UserKey adminKey;
    private UserKey managerKey;

    public ElementIntegrationTests() {
    }

    @Autowired
    private void setDB(EntityFactory factory, EnhancedUserDao<UserKey> userDao, EnhancedElementDao<ElementKey> elementDao){
        this.factory = factory;
        this.userDao = userDao;
        this.elementDao = elementDao;
    }

    @LocalServerPort
    public void setPort(int port) {
        this.port = port;
    }

    @PostConstruct
    public void init() {
        this.baseUrl = "http://localhost:" + port + "/smartspace";
        this.restTemplate = new RestTemplate();
    }

    @Before
    public void onStart() {
        this.userDao.deleteAll();
        this.elementDao.deleteAll();
        this.adminKey = createAdmin();
        this.managerKey = createManager();
    }

    private UserKey createAdmin() {
        UserEntity adminEntity = factory.createNewUser(
                "alon@gmail.com",
                "2019BTal.Cohen",
                "gsd",
                "Gf",
                UserRole.ADMIN,
                100L);
        userDao.create(adminEntity);
        return adminEntity.getKey();
    }

    private ElementKey generateElementKey() {
        ElementKey key = new ElementKey();
        key.setId("Bla" + (++counter));
        key.setSmartspace("mySmartSpace");
        return key;
    }

    private UserKey createManager() {
        UserEntity managerEntity = factory.createNewUser(
                "alonSamay@gmail.com",
                "2019BTal.Cohen",
                "gsd",
                "Gf",
                UserRole.MANAGER,
                100L);
        userDao.create(managerEntity);
        return managerEntity.getKey();
    }


    @After
    public void tearDown() {
        this.userDao.deleteAll();
        this.elementDao.deleteAll();
    }

    @Test
    public void testElementsPostRequest() {
        // GIVEN admin user in the database
        UserKey adminKey = createAdmin();

        // WHEN 10 element boundaries are posted to the server
        int totalSize = 10;
        Map<String, Object> details = new HashMap<>();
        details.put("key1", "hello ");
        Location loc = new Location(10, 10);

        List<ElementBoundary> allElements =
                IntStream
                        .range(1, totalSize + 1)
                        .mapToObj(i -> factory.createNewElement(
                                "name #" + i,
                                "myType",
                                loc,
                                new Date(),
                                "test@gmail.com",
                                "mySmartSpace",
                                false,
                                details))
                        .peek(element -> element.setKey(generateElementKey()))
                        .map(ElementBoundary::new)
                        .collect(Collectors.toList());

        List<ElementEntity> actualResult =
                Arrays.stream(Objects.requireNonNull(
                        this.restTemplate.postForObject(
                                this.baseUrl + ADMIN_URL,
                                allElements,
                                ElementBoundary[].class,
                                adminKey.getSmartspace(),
                                adminKey.getEmail())))
                        .map(ElementBoundary::convertToEntity)
                        .collect(Collectors.toList());

        List<ElementEntity> expected =
                actualResult
                        .stream()
                        .skip(4)
                        .limit(5)
                        .collect(Collectors.toList());

        // THEN the result contains 5 elements of the elements inserted to the database
        int page = 1;
        int size = 5;
        assertThat(elementDao.readAll(size, page))
                .usingElementComparatorOnFields("key")
                .containsExactlyInAnyOrderElementsOf(expected);
    }


    @Test
    public void testGetAllUsingPagination() {
        // GIVEN 10 elements in the database and admin user
        UserKey adminKey = createAdmin();

        Map<String, Object> details = new HashMap<>();
        details.put("key1", "hello ");

        int totalSize = 10;
        List<ElementBoundary> allElements =
                IntStream
                        .range(1, totalSize + 1)
                        .mapToObj(i -> factory.createNewElement(
                                "name #" + i,
                                "myType",
                                new Location(4.5, 3.2),
                                new Date(),
                                "test@gmail.com",
                                "mySmartSpace",
                                false,
                                details))
                        .peek(element -> element.setKey(generateElementKey()))
                        .map(this.elementDao::create)
                        .map(ElementBoundary::new)
                        .collect(Collectors.toList());

        List<ElementBoundary> expected =
                allElements
                        .stream()
                        .skip(4)
                        .limit(5)
                        .collect(Collectors.toList());

        // WHEN I get all users using page 1 and size 5
        int page = 1;
        int size = 5;
        ElementBoundary[] result =
                this.restTemplate
                        .getForObject(
                                this.baseUrl + ADMIN_URL + "?size={size}&page={page}",
                                ElementBoundary[].class,
                                adminKey.getSmartspace(),
                                adminKey.getEmail(),
                                size,
                                page);

        // THEN the result contains 5 users of the users inserted to the database
        assertThat(result)
                .usingElementComparatorOnFields("key")
                .containsExactlyInAnyOrderElementsOf(expected);
    }

    @Test
    public void testAddElement() {
        // GIVEN a manager user in the db
        UserKey managerKey = createManager();

        // WHEN adding new element without element key
        ElementEntity entity = factory.createNewElement(
                "hello",
                "good",
                new Location(4.2, 3.2),
                new Date(),
                "samay@gmail.com",
                "mysmartspace",
                false,
                new HashMap<>());
        ElementBoundary boundary = new ElementBoundary(entity);

        ElementBoundary boundaryFromDB = restTemplate.postForObject(
                this.baseUrl + MANAGER_URL,
                boundary,
                ElementBoundary.class,
                managerKey.getSmartspace(),
                managerKey.getEmail());

        // THEN the new element is added to the DB
        assertThat(boundary).isEqualToIgnoringGivenFields(boundaryFromDB, "key", "created");
    }

    @Test
    public void testUpdateElement() {
        // GIVEN a manager user and an element in the db
        UserKey managerKey = createManager();

        ElementEntity entity =
                factory.createNewElement(
                        "hello",
                        "good",
                        new Location(4.2, 3.2),
                        new Date(),
                        "samay@gmail.com",
                        "mysmartspace",
                        false,
                        new HashMap<>());
        ElementKey elementKey = new ElementKey("32", managerKey.getSmartspace());
        entity.setKey(elementKey);
        elementDao.create(entity);

        // WHEN updating exist element
        entity.setName("goodbye");
        entity.setLocation(new Location(5.4, 3));

        ElementBoundary boundary = new ElementBoundary(entity);
        boundary.setKey(null); // the updated boundary key should be null by spec

        restTemplate.put(
                this.baseUrl + MANAGER_URL + ELEMENTS_URL,
                boundary,
                managerKey.getSmartspace(),
                managerKey.getEmail(),
                elementKey.getSmartspace(),
                elementKey.getId());

        // THEN the new element is updated in DB
        ElementEntity updatedEntity =
                this.elementDao
                        .readById(elementKey)
                        .orElse(null);

        assertThat(entity)
                .isEqualToIgnoringGivenFields(updatedEntity, "key", "location");
    }

    @Test
    public void testGetSpecificElement() {
        // GIVEN a manager user and an element in the db
        UserKey managerKey = createManager();

        ElementEntity entity = factory.createNewElement(
                "hello",
                "good",
                new Location(4.2, 3.2),
                new Date(),
                managerKey.getEmail(),
                managerKey.getSmartspace(),
                false,
                new HashMap<>());
        ElementKey elementKey = new ElementKey("32", managerKey.getSmartspace());
        entity.setKey(elementKey);
        elementDao.create(entity);

        // WHEN invoking get for that element
        ElementBoundary boundary =
                restTemplate
                        .getForObject(
                                this.baseUrl + USER_URL + ELEMENTS_URL,
                                ElementBoundary.class,
                                managerKey.getSmartspace(),
                                managerKey.getEmail(),
                                elementKey.getSmartspace(),
                                elementKey.getId());

        // THEN the element is gotten
        assertThat(entity)
                .isEqualToComparingFieldByFieldRecursively(boundary != null ? boundary.convertToEntity() : null);
    }

}