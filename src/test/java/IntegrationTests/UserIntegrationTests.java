package IntegrationTests;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import smartspace.Application;
import smartspace.dao.EnhancedUserDao;
import smartspace.data.UserEntity;
import smartspace.data.UserKey;
import smartspace.data.UserRole;
import smartspace.data.util.EntityFactory;
import smartspace.layout.UserBoundary;
import smartspace.layout.UserForm;
import smartspace.layout.exceptions.NotFoundException;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"spring.profiles.active=default"})
public class UserIntegrationTests {

    private static final String ADMIN_URL = "/admin/users/{adminSmartspace}/{adminEmail}";
    private static final String LOGIN_URL = "/login/{userSmartspace}/{userEmail}";
    private static final String USER_URL = "/users";

    private int port;
    private String baseURL;

    private EntityFactory factory;
    private EnhancedUserDao<UserKey> userDao;
    private RestTemplate restTemplate;

    private static int counter = 0;

    @Autowired
    public void setUserDao(EnhancedUserDao<UserKey> userDao) {
        this.userDao = userDao;
    }

    @LocalServerPort
    public void setPort(int port) {
        this.port = port;
    }

    @Autowired
    public void setFactory(EntityFactory factory) {
        this.factory = factory;
    }

    @PostConstruct
    public void init() {
        this.baseURL = "http://localhost:" + port + "/smartspace";
        this.restTemplate = new RestTemplate();
    }

    private UserKey createAdmin() {
        UserEntity adminEntity = factory.createNewUser(
                "alon@gmail.com",
                "2019BTal.Cohen",
                "gsd",
                "Gf",
                UserRole.ADMIN,
                100L);
        userDao.create(adminEntity);
        return adminEntity.getKey();
    }


    public UserKey generateUserKey() {
        UserKey key = new UserKey();
        key.setSmartspace("Bla" + (++counter));
        key.setEmail("bla@gmail.com");
        return key;
    }

    @After
    public void tearDown() {
        this.userDao.deleteAll();
    }

    @Test
    public void testUsersPostRequest() {
        // GIVEN admin in the database
        UserKey adminKey = createAdmin();

        // WHEN 10 user are imported to the server
        int totalSize = 10;

        List<UserBoundary> allUsers =
                IntStream
                        .range(1, totalSize + 1)
                        .mapToObj(i -> factory.createNewUser(
                                String.format("user#%d@gmail.com", i),
                                "smart",
                                "user #" + i,
                                ":)",
                                UserRole.PLAYER,
                                200L))
                        .map(UserBoundary::new)
                        .collect(Collectors.toList());

        List<UserBoundary> actualResult =
                Arrays.stream(Objects.requireNonNull(
                        this.restTemplate.postForObject(
                                this.baseURL + ADMIN_URL,
                                allUsers,
                                UserBoundary[].class,
                                adminKey.getSmartspace(),
                                adminKey.getEmail())))
                        .collect(Collectors.toList());

        // THEN the database contains the new users
        assertThat(allUsers)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyElementsOf(actualResult);
    }

    @Test
    public void testGetAllUsingPagination() {
        // GIVEN admin and 10 messages in the DB
        UserKey adminKey = createAdmin();

        int totalSize = 10;

        List<UserBoundary> allUsers =
                IntStream
                        .range(1, totalSize + 1)
                        .mapToObj(i -> factory.createNewUser(
                                String.format("user#%d@gmail.com", i),
                                "smart",
                                "user #" + i,
                                ":)",
                                UserRole.PLAYER,
                                200L))
                        .map(this.userDao::create)
                        .map(UserBoundary::new)
                        .collect(Collectors.toList());

        List<UserBoundary> expected =
                allUsers
                        .stream()
                        .skip(2)
                        .limit(5)
                        .collect(Collectors.toList());

        // WHEN I get all users using page 1 and size 5
        int page = 1;
        int size = 5;
        UserBoundary[] result =
                this.restTemplate
                        .getForObject(
                                this.baseURL + ADMIN_URL + "?size={size}&page={page}",
                                UserBoundary[].class,
                                adminKey.getSmartspace(),
                                adminKey.getEmail(),
                                size,
                                page);

        // THEN the result contains 5 users of the users inserted to the database
        assertThat(result)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyElementsOf(expected);
    }

    @Test
    public void testNewUser() {
        // GIVEN nothing

        // WHEN I create new user using user form
        UserForm userForm =
                new UserForm(
                        "samay.alon@gmail.com",
                        "PLAYER",
                        "AloNs",
                        "T_T");

        UserBoundary boundary =
                this.restTemplate
                        .postForObject(
                                this.baseURL + USER_URL,
                                userForm,
                                UserBoundary.class);

        // THEN the new user is added to the DB
        assertThat(boundary)
                .isEqualToComparingOnlyGivenFields(
                        userForm,
                        "role",
                        "username",
                        "avatar");
    }

    private UserEntity createUser() {
        UserEntity user = factory.createNewUser(
                "alon@gmail.com",
                "kk",
                "AlonSamay",
                ":)",
                UserRole.PLAYER,
                150L);
        this.userDao.create(user);
        return user;
    }

    @Test
    public void testUpdateExistingUser() {
        // GIVEN a user in the database
        UserEntity user = createUser();
        long points = user.getPoints();

        // WHEN I update the username, avatar and points
        user.setUsername("AloNs");
        user.setAvatar(":/");
        user.setPoints(points + 100);

        restTemplate
                .put(
                        this.baseURL + USER_URL + LOGIN_URL,
                        new UserBoundary(user),
                        user.getKey().getSmartspace(),
                        user.getKey().getEmail());

        user.setPoints(points);

        // THEN all attributes with changes (without points) will update
        Optional<UserEntity> entity = userDao.readById(user.getKey());
        if (!entity.isPresent()) {
            throw new NotFoundException(this.getClass().getSimpleName() + ": Can't find user with given key");
        }

        UserEntity userEntity = entity.get();
        assertThat(user)
                .isEqualToIgnoringGivenFields(userEntity, "username", "avatar");
    }

    @Test
    public void testGetUser() {
        // GIVEN a user in the database
        UserEntity user = createUser();

        // WHEN I invoke get request of user
        UserEntity entityFromDB =
                Objects.requireNonNull(
                        restTemplate
                                .getForObject(
                                        baseURL + USER_URL + LOGIN_URL,
                                        UserBoundary.class,
                                        user.getKey().getSmartspace(),
                                        user.getKey().getEmail()))
                        .convertToEntity();

        // THEN I get the exact user
        assertThat(user).isEqualToComparingFieldByFieldRecursively(entityFromDB);
    }
}
