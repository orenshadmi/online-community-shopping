package IntegrationTests;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import smartspace.Application;
import smartspace.dao.EnhancedActionDao;
import smartspace.dao.EnhancedElementDao;
import smartspace.dao.EnhancedUserDao;
import smartspace.data.*;
import smartspace.data.util.EntityFactory;
import smartspace.layout.ActionBoundary;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"spring.profiles.active=default"})
public class ActionIntegrationTests {

    private static final String ADMIN_URL = "/admin/actions/{adminSmartspace}/{adminEmail}";
    private static final String ACTIONS_URL = "/actions";

    private String baseUrl;
    private int port;

    private RestTemplate restTemplate;

    private EntityFactory factory;
    private EnhancedElementDao<ElementKey> elementDao;
    private EnhancedActionDao actionDao;
    private EnhancedUserDao<UserKey> userDao;

    private static int counter = 0;
    private ElementKey elementKey;

    public ActionIntegrationTests() {
    }

    @Autowired
    public void setDB(EntityFactory factory, EnhancedElementDao<ElementKey> elementDao, EnhancedActionDao actionDao, EnhancedUserDao<UserKey> userDao) {
        this.factory = factory;
        this.elementDao = elementDao;
        this.actionDao = actionDao;
        this.userDao = userDao;
    }

    @LocalServerPort
    public void setPort(int port) {
        this.port = port;
    }

    @PostConstruct
    public void init() {
        this.baseUrl = "http://localhost:" + port + "/smartspace";
        this.restTemplate = new RestTemplate();
    }

    @Before
    public void setup() {
        actionDao.deleteAll();
        elementKey = createElement();
    }

    @After
    public void tearDown() {
        elementDao.deleteAll();
        userDao.deleteAll();
    }

    private ElementKey createElement() {
        ElementEntity elementEntity = factory.createNewElement("a",
                "ab",
                new Location(5.4, 3.7),
                new Date(),
                "fsda@gmail.com",
                "gfsd",
                false,
                null);
        return elementDao.create(elementEntity).getKey();
    }

    private UserKey createAdmin() {
        UserEntity adminEntity = factory.createNewUser(
                "alon@gmail.com",
                "2019BTal.Cohen",
                "gsd",
                "Gf",
                UserRole.ADMIN,
                100L);
        return userDao.create(adminEntity).getKey();
    }

    @Test
    public void testImportActions() {
        // GIVEN element and admin in database
        UserKey adminKey = createAdmin();

        // WHEN 10 action boundaries are posted to the server
        int totalSize = 10;

        Map<String, Object> details = new HashMap<>();
        details.put("key1", "hello ");

        List<ActionBoundary> allActions =
                IntStream
                        .range(1, totalSize + 1)
                        .mapToObj(i -> factory.createNewAction(
                                elementKey.getId(),
                                elementKey.getSmartspace(),
                                "Py",
                                new Date(),
                                "fda@gmail.com",
                                "space",
                                details))
                        .peek(action -> action.setKey(generateActionKey()))
                        .map(ActionBoundary::new)
                        .collect(Collectors.toList());

        List<ActionBoundary> actualResult =
                Arrays.stream(Objects.requireNonNull(
                        this.restTemplate.postForObject(
                                this.baseUrl + ADMIN_URL,
                                allActions,
                                ActionBoundary[].class,
                                adminKey.getSmartspace(),
                                adminKey.getEmail())))
                        .collect(Collectors.toList());

        assertThat(allActions)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrderElementsOf(actualResult);
    }

    private ActionKey generateActionKey() {
        return new ActionKey("" + (++counter), "mysmartspace");
    }

    @Test
    public void testGetAllUsingPagination() {
        // GIVEN database which contains 10 actions, 1 element and 1 admin
        UserKey adminKey = createAdmin();

        int totalSize = 10;

        Map<String, Object> details = new HashMap<>();
        details.put("key1", "hello ");

        List<ActionBoundary> allUsers =
                IntStream
                        .range(1, totalSize + 1)
                        .mapToObj(i -> factory.createNewAction(
                                elementKey.getId(),
                                elementKey.getSmartspace(),
                                "Py",
                                new Date(),
                                "fda@gmail.com",
                                "space",
                                details))
                        .map(actionDao::create)
                        .map(ActionBoundary::new)
                        .collect(Collectors.toList());

        List<ActionBoundary> expected =
                allUsers
                        .stream()
                        .skip(5)
                        .limit(5)
                        .collect(Collectors.toList());

        // WHEN I get all users using page 1 and size 5
        int page = 1;
        int size = 5;
        ActionBoundary[] result =
                this.restTemplate
                        .getForObject(
                                this.baseUrl + ADMIN_URL + "?size={size}&page={page}",
                                ActionBoundary[].class,
                                adminKey.getSmartspace(),
                                adminKey.getEmail(),
                                size,
                                page);

        // THEN the result contains 5 users of the users inserted to the database
        assertThat(result)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrderElementsOf(expected);
    }

    @Test
    public void testActionInvoke() {
        // GIVEN element in the DB

        // WHEN I invoke post of an action on the element
        ActionEntity entity = factory.createNewAction(
                elementKey.getId(),
                elementKey.getSmartspace(),
                "bad",
                new Date(),
                "samay@gmail.com",
                "smartspace",
                new HashMap<>());
        ActionBoundary boundary = new ActionBoundary(entity);
        ActionBoundary boundaryFromDB =
                restTemplate
                        .postForObject(
                                baseUrl + ACTIONS_URL,
                                boundary,
                                ActionBoundary.class);

        // THEN the action will be added into the DB
        assertThat(boundary).isEqualToIgnoringGivenFields(boundaryFromDB, "actionKey");
    }
}
