package smartspace.data;

import java.util.Date;

public class PaymentInfo {

    private long cardNumber;
    private String cardOwnerName;
    private Date exparationDate;
    private int CVV;

    public PaymentInfo(){

    }
    public PaymentInfo(int cardNumber , String cardOwnerName , Date exparationDate , int CVV){
        this.cardNumber = cardNumber;
        this.cardOwnerName = cardOwnerName;
        this.exparationDate = exparationDate;
        this.CVV = CVV;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardOwnerName() {
        return cardOwnerName;
    }

    public void setCardOwnerName(String cardOwnerName) {
        this.cardOwnerName = cardOwnerName;
    }

    public Date getExparationDate() {
        return exparationDate;
    }

    public void setExparationDate(Date exparationDate) {
        this.exparationDate = exparationDate;
    }

    public int getCVV() {
        return CVV;
    }

    public void setCVV(int CVV) {
        this.CVV = CVV;
    }
}
