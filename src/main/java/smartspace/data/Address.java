package smartspace.data;

public class Address {

    private String street;
    private String city;
    private String zipCode;
    private String phone;

    public Address(){

    }

    public Address(String street, String city, String zipCode, String phone) {
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
