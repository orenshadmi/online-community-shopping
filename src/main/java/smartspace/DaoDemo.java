package smartspace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import smartspace.dao.*;
import smartspace.dao.nonrdb.nonRdbActionDao;
import smartspace.dao.nonrdb.nonRdbElementDao;
import smartspace.dao.nonrdb.nonRdbUserDao;
import smartspace.data.*;
import smartspace.data.util.EntityFactory;
import smartspace.layout.exceptions.NotFoundException;

import java.util.*;


@Component
@PropertySource("fields.properties")
public class DaoDemo implements CommandLineRunner {
    private nonRdbElementDao nonRdbElementDao;
    private nonRdbActionDao nonRdbActionDao;
    private nonRdbUserDao nonRdbUserDao;
    private IdGeneratorCrud idGeneratorCrud;
    private EntityFactory factory;
    ArrayList<String> productsInCatalong;

    @Value("${SmartSpace.name.property}")
    private String smartSpaceName;
    private String managerMail;

    @Autowired
    private Environment env;

    @Autowired
    public DaoDemo(nonRdbUserDao nonRdbUserDao,
                   nonRdbActionDao nonRdbActionDao,
                   nonRdbElementDao nonRdbElementDao,
                   IdGeneratorCrud idGeneratorCrud,
                   EntityFactory factory) {
        super();
        this.nonRdbUserDao = nonRdbUserDao;
        this.nonRdbActionDao = nonRdbActionDao;
        this.nonRdbElementDao = nonRdbElementDao;
        this.idGeneratorCrud = idGeneratorCrud;
        this.factory = factory;
        this.managerMail = "Alon@gmail.com";

    this.nonRdbUserDao.deleteAll();
    this.nonRdbActionDao.deleteAll();
    this.nonRdbElementDao.deleteAll();
    }


    @Override
    public void run(String... args) throws Exception {

        createUsers();
        createElements();
        createActions();


//		readAllUsers();
//		readAllElements();
//		readAllActions();

        String name = "cart";
        readAllByName(name);

        String type = "product";
        readAllByType(type);

        ElementKey elementKey = new ElementKey("6", this.smartSpaceName);
        readElementById(elementKey);

    }


    private void readAllByType(String type) {
        nonRdbElementDao.readAllByType(type, 4, 0)
                .forEach(element -> System.err.println(element));
    }

    private void readAllByName(String name) {
        nonRdbElementDao.readAllByName(name, 4, 0)
                .forEach(element -> System.err.println(element));
    }

    private void readAllActions() {
        nonRdbActionDao
                .readAll(10, 0) // list
                .forEach(action -> System.err.println(action));
    }

    private void readAllUsers() {
        nonRdbUserDao.readAll(10, 0)// list
                .forEach(user -> System.err.println(user));
    }

    private void readAllElements() {
        nonRdbElementDao
                .readAll(10, 0) // list
                .forEach(element -> System.err.println(element));
    }

    private void updateEntity(ElementEntity elementEntity) {
        elementEntity.setCreatorEmail("Test33");
        nonRdbElementDao.update(elementEntity);
        System.err.print("After " + nonRdbElementDao.readById(elementEntity.getKey()).get().toString());
    }

    private void updateUser(UserEntity userEntity) {
        userEntity.setUsername("TEST33");
        nonRdbUserDao.update(userEntity);
        System.err.print("After " + nonRdbUserDao.readById(userEntity.getKey()));
    }

    private UserEntity readUserById() {
        UserEntity userEntity = nonRdbUserDao.readById(new UserKey("a@gmail.com")).get();
        System.err.print(userEntity);
        return userEntity;
    }

    private ElementEntity readElementById(ElementKey elementKey) {
        Optional<ElementEntity> optEntity = nonRdbElementDao.readById(elementKey);

        if (!optEntity.isPresent()) {
            throw new NotFoundException(this.getClass().getSimpleName() + ": Can't find element with key: " + elementKey);
        }
        System.err.print(optEntity.get());
        return optEntity.get();
    }

    private void createActions() {

        ActionEntity likeProduct1 = factory.createNewAction(
                "2",
                this.smartSpaceName,
                "like",
                new Date(),
                "talcohen19@gmail.com",
                smartSpaceName,
                new HashMap<>());
        this.nonRdbActionDao.create(likeProduct1);

        ActionEntity likeProduct2 = factory.createNewAction(
                "3",
                this.smartSpaceName,
                "like",
                new Date(),
                "talcohen19@gmail.com",
                smartSpaceName,
                new HashMap<>());
        this.nonRdbActionDao.create(likeProduct2);


        HashMap<String, Object> moreAtt = new HashMap<>();
        moreAtt.put("Products", Arrays.toString(new String[]{"2,3"}));
        moreAtt.put("Amount", "57");
        ActionEntity addToCart = factory.createNewAction(
                "5",
                this.smartSpaceName,
                "addToCart",
                new Date(),
                "talcohen19@gmail.com",
                this.smartSpaceName, moreAtt);
        this.nonRdbActionDao.create(addToCart);

    }

    private void createElements() {
        HashMap<String, Object> moreAtt = new HashMap<>();
        productsInCatalong = new ArrayList<String>();
        String[] names = {
                "Diesel leather logo buckle belt in black",
                "Wood Wood Alex trainer in white",
                "Sweetness block heel bow mules in berry",
                "adidas Originals mini logo t-shirt dress in black",
                "Superdry stripe bodycon dress",
                "JDY photographic t-shirt",
                "Reclaimed Vintage The 89' slim leg jean",
                "Recycled Florence authentic straight leg jeans",
                "BCBGeneration patent black clutch bag",
                "Superdry Elite hooded windcheater jacket",
                "sliders in black",
                "swim shorts in water gun print short length",
                "rimless cat eye sunglasses in yellow",
                "midweight chain in gunmetal finish",
                "rolltop backpack in black with double straps and front pocket",
                "faux leather oval cross body bum bag with all over print in brown",
                "cargo shorts with contrast stitch",
                "utility jacket in bright blue",
                "slim shorts in washed pink with cargo pocket",
                "trainers in black mix with chunky sole"

        };
        Double[] prices = {
                14.90,
                22.90,
                27.50,
                25.90,
                34.90,
                13.00,
                21.90,
                24.90,
                31.90,
                45.50,
                12.50,
                23.25,
                14.48,
                8.49,
                36.50,
                17.25,
                35.99,
                58.50,
                31.90,
                65.50
        };
        String[] srcs = {
                "https://images.asos-media.com/products/diesel-leather-logo-buckle-belt-in-black/12461646-1-black?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/wood-wood-alex-trainer-in-white/9277174-1-white?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-sweetness-block-heel-bow-mules-in-berry/11623767-1-berry?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/adidas-originals-mini-logo-t-shirt-dress-in-black/11711900-1-black?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/superdry-stripe-bodycon-dress/12059438-1-blueyellowstripe?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/jdy-photographic-t-shirt/12507402-1-clouddancerwprint?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/reclaimed-vintage-the-89-slim-leg-jean-with-reworked-and-distressed-seams/11587567-1-acidwash?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-recycled-florence-authentic-straight-leg-jeans-in-aged-stonewash-blue/9831382-1-lightstonewashblue?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/bcbgeneration-patent-black-clutch-bag/12376728-1-blackpatent?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/superdry-elite-hooded-windcheater-jacket/12105644-1-hyperredblack?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-sliders-in-all-over-camo-print/10779382-1-black?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-swim-shorts-in-water-gun-print-short-length/11718460-1-black?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-rimless-cat-eye-sunglasses-in-yellow/11076550-1-yellow?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-midweight-chain-in-gunmetal-finish/12029805-1-gunmetal?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-rolltop-backpack-in-black-with-double-straps-and-front-pocket/11079380-1-black?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-faux-leather-oval-cross-body-bum-bag-with-all-over-print-in-brown/10751700-1-brown?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-cargo-shorts-with-contrast-stitch/11407452-1-navy?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-utility-jacket-in-bright-blue/11154520-1-bluewash?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-slim-shorts-in-washed-pink-with-cargo-pocket/11407446-1-washedpink?$n_320w$&wid=317&fit=constrain",
                "https://images.asos-media.com/products/asos-design-trainers-in-black-mix-with-chunky-sole/11033707-1-black?$n_320w$&wid=317&fit=constrain"
        };
        String[] descriptions = {
                "Belt by Diesel \nSmooth leather \nYou can't beat a classic \nMain: 100% Real Leather.",
                "Trainers by Wood Wood \nLeather upper \nIt's the real deal \nLining: 100% Real Leather, Sole: 100% Rubber, Upper: 100% Real Leather.",
                "Mules by ASOS DESIGN \nSmooth velvet \nFor added softness and a bit of luxury \nLining Sock: 50% Other Materials, 50% Textile, Sole: 100% Other Materials, Upper: 100% Textile.",
                "Mini dress by adidas \nSoft jersey \nadidas partners with the Better Cotton Initiative to improve cotton farming globally \nThis makes it better for farmers and the environment \nBCI provides farming-practice training \nIt promotes things like water efficiency and reducing the most harmful chemicals \n Body: 100% Cotton.",
                "Dress by Superdry \nRibbed jersey \nSoft-touch style \nBody: 49% Cotton, 31% Polyester, 15% Viscose, 5% Elastane.",
                "T-shirt by JDY \nGo-with-everything jersey \nIt's a soft all-rounder \nBody: 65% Polyester, 35% Viscose.",
                "Jeans by Reclaimed Vintage \nNon-stretch denim \nThick, structured cotton \nMain: 100% Cotton.",
                "Jeans by ASOS DESIGN \nNon-stretch denim \nHigh-quality mix of recycled denim and Cotton made in Africa (CmiA)\nPart of our commitment to reducing the water, CO2 and energy we use \nThis pair of jeans has saved on average 623 litres of water \nThat's about 3115 cups of coffee \nDon’t just take our word for it \nScan the label inside to find out more \nMain: 100% Cotton.",
                "Bag by BCBGeneration \nPatent faux leather\nShiny, glossy coating \nMain: 100% Polyurethane.",
                "Jacket by Superdry \nSmooth woven fabric \nThe kind that doesn't stretch \nBody: 100% Polyester.",
                "Sandals by ASOS DESIGN \nLining: 50% Other Materials, 50% Textile, Sole: 100% Other Materials, Upper: 100% Other Materials. ",
                "Swim shorts by ASOS DESIGN \nLightweight swim fabric \nMade from recycled polyester \nPlastic bottles and textile waste are processed into plastic chips and melted into new fibres " + "\nThis saves water and energy and reduces greenhouse gas emissions, too \nCool, right? \nLining: 100% Polyester, Main: 100% Polyester.",
                "Sunglasses by ASOS DESIGN \nFrame: 100% Plastic, Lens: 100% Plastic.\nFC0 Light tint sunglasses, very limited reduction of sunglare. \nThese sunglasses conform to BSENISO12312-1:2013. \nNot for direct viewing of the sun or protection against artificial light sources such as in a solarium. \nNon prescription sunglasses. \nNot for use as eye protection against mechanical impact. \nDo not use abrasive detergents or sprays.",
                "Necklace by ASOS DESIGN \nNecklace: 100% Steel.",
                "Backpack by ASOS DESIGN \nTightly woven fabric \nSlightly structured, very versatile \nLining: 100% Polyester, Main: 100% Polyester.",
                "Bum bag by ASOS DESIGN \nFaux leather \nLet's be honest, no one can tell the difference \nCoating: 100% Polyurethane, Lining: 100% Polyester, Main: 100% Polyester.",
                "Shorts by ASOS DESIGN \nPure cotton \nIt's light and breathable \nMain: 100% Cotton.",
                "Jacket by ASOS DESIGN \nLightweight woven fabric \nIt'll see you through those tricky transitional months \nLining: 100% Polyester, Shell: 100% Nylon.",
                "Shorts by ASOS DESIGN \nPure cotton \nIt's light and breathable \nMain: 100% Cotton.",
                "Trainers by ASOS DESIGN \nFaux leather and textile upper \nComes together nicely \nLining: 100% Textile, Sole: 100% Other Materials, Upper: 50% Other Materials, 50% Textile."
        };

        String[] sizes = {
                "S",
                "42",
                "39",
                "M",
                "S",
                "S",
                "S",
                "M",
                "S",
                "M",
                "43",
                "L",
                "S",
                "M",
                "L",
                "S",
                "L",
                "M",
                "L",
                "45"
        };
        String[] colors = {
                "Black",
                "White",
                "Red",
                "Black",
                "Green, Blue, Yellow",
                "White",
                "Blue",
                "Blue",
                "Black",
                "Pink",
                "Camo",
                "Black",
                "Yellow",
                "Metal",
                "Black",
                "Brown",
                "Navy",
                "Blue",
                "Pink",
                "Black"

        };

        for (int i = 0; i < prices.length; i++) {
            moreAtt.put(env.getProperty("fields.product.price"), prices[i]);
            moreAtt.put(env.getProperty("fields.product.src"), srcs[i]);
            moreAtt.put(env.getProperty("fields.product.description"), descriptions[i]);
            moreAtt.put(env.getProperty("fields.product.size"), sizes[i]);
            moreAtt.put(env.getProperty("fields.product.color"), colors[i]);
            moreAtt.put(env.getProperty("fields.product.totalLikes"), i);
            moreAtt.put(env.getProperty("fields.product.amount"), 0);
            ElementEntity product1 = factory.createNewElement(
                    names[i],
                    "product",
                    new Location(32.113165 + (i * 0.0001), 34.818044 + (i * 0.0001)),
                    new Date(),
                    "yanai18@gmail.com",
                    this.smartSpaceName,
                    false,
                    moreAtt);
            ElementKey product1Key = new ElementKey(String.valueOf(i + 1), this.smartSpaceName);
            product1.setElementKey(product1Key);
            this.nonRdbElementDao.create(product1);
            productsInCatalong.add(String.valueOf(i + 1));

            moreAtt.clear();
        }

        moreAtt.put("Products", productsInCatalong.toString());
        ElementEntity catalog = factory.createNewElement(
                "AsosCatalog",
                "Catalog",
                new Location(1.0, 2.0),
                new Date(),
                managerMail,
                this.smartSpaceName,
                false,
                moreAtt);
        ElementKey catalogKey = new ElementKey("0", this.smartSpaceName);
        catalog.setElementKey(catalogKey);
        this.nonRdbElementDao.create(catalog);

        moreAtt.clear();


        ElementEntity cartGenerator = factory.createNewElement(
                "cartGenerator",
                "cartGenerator",
                new Location(0.0, 0.0),
                new Date(),
                managerMail,
                this.smartSpaceName,
                false,
                moreAtt);
        ElementKey cartGeneratorKey = new ElementKey("21", this.smartSpaceName);
        cartGenerator.setElementKey(cartGeneratorKey);
        this.nonRdbElementDao.create(cartGenerator);

        moreAtt.clear();

        String[] emails = {"talcohen@gmail.com", "tomrozanski@gmail.com", "yanailipshitz@gmail.com", "maork@gmail.com"};

        for (int i = 0; i < emails.length; i++) {
            moreAtt.clear();
            moreAtt.put(env.getProperty("fields.cart.cartOwnerId"), new UserKey(emails[i]));
            moreAtt.put(env.getProperty("fields.cart.products"), new String[]{});
            moreAtt.put(env.getProperty("fields.cart.amount"), 0);
            moreAtt.put(env.getProperty("fields.cart.address"), new Address());
            moreAtt.put(env.getProperty("fields.cart.paymentinfo"), new PaymentInfo());

            ElementEntity cart = factory.createNewElement(
                    "cart" + (i + 22),
                    "cart",
                    new Location(3.0, 3.0),
                    new Date(),
                    "tom19@gmail.com",
                    this.smartSpaceName,
                    false,
                    moreAtt);

            ElementKey cartKey = new ElementKey(String.valueOf(i + 22), this.smartSpaceName);
            cart.setElementKey(cartKey);
            this.nonRdbElementDao.create(cart);

        }


    }

    private void createUsers() {
        UserEntity player1 = factory.createNewUser(
                "talCohen@gmail.com",
                this.smartSpaceName,
                "talCohen",
                "!TAL!",
                UserRole.PLAYER,
                (long) 123
        );
        this.nonRdbUserDao.create(player1);

        UserEntity player2 = factory.createNewUser(
                "tomrozanski@gmail.com",
                this.smartSpaceName,
                "tomRozanski",
                "@TOM@",
                UserRole.PLAYER,
                (long) 123
        );
        this.nonRdbUserDao.create(player2);

        UserEntity player3 = factory.createNewUser(
                "yanailipshitz@gmail.com",
                this.smartSpaceName,
                "yanaiLipshitz",
                "^_^",
                UserRole.PLAYER,
                (long) 123
        );
        this.nonRdbUserDao.create(player3);

        UserEntity player4 = factory.createNewUser(
                "maork@gmail.com",
                this.smartSpaceName,
                "maork",
                ";-}",
                UserRole.PLAYER,
                (long) 123
        );
        this.nonRdbUserDao.create(player4);

        UserEntity manager = factory.createNewUser(
                this.managerMail,
                this.smartSpaceName,
                "AlonSamay",
                ":)",
                UserRole.MANAGER,
                (long) 456);
        this.nonRdbUserDao.create(manager);


        UserEntity admin = factory.createNewUser(
                "orenShadmi@gmail.com",
                this.smartSpaceName,
                "OrenShadmi",
                "-OREN-",
                UserRole.ADMIN,
                (long) 123);
        this.nonRdbUserDao.create(admin);

        UserEntity manager1 = factory.createNewUser(
                "yanai18@gmail.com",
                this.smartSpaceName,
                "Yanai18",
                "o_0",
                UserRole.MANAGER,
                (long) 900);
        this.nonRdbUserDao.create(manager1);

        UserEntity manager2 = factory.createNewUser(
                "tom19@gmail.com",
                this.smartSpaceName,
                "Tom19",
                "0_o",
                UserRole.MANAGER,
                (long) 900);
        this.nonRdbUserDao.create(manager2);
    }

}
