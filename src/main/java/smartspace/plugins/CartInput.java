package smartspace.plugins;

import smartspace.data.Address;
import smartspace.data.PaymentInfo;
import smartspace.data.UserKey;

import java.util.Date;

public class CartInput {
    private UserKey cartOwnerId;
    private String[] products;
    private float amount;
    private Address address;
    private PaymentInfo paymentInfo;


    public CartInput() {
    }
    public CartInput(UserKey cartOwnerId){
//        this.cartOwnerId = cartOwnerId;
//        this.amount=0;
//        this.address ="N/A";

        this.cartOwnerId = cartOwnerId;
        this.amount = 0;
        this.address = new Address();
        this.paymentInfo = new PaymentInfo();
    }

    public UserKey getCartOwnerId() {
        return cartOwnerId;
    }

    public void setCartOwnerId(UserKey cartOwnerId) {
        this.cartOwnerId = cartOwnerId;
    }

    public String[] getProducts() {
        return products;
    }

    public void setProducts(String[] products) {
        this.products = products;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }
}



