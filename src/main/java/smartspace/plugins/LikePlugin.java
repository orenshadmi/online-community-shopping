package smartspace.plugins;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import smartspace.dao.nonrdb.nonRdbActionDao;
import smartspace.dao.nonrdb.nonRdbElementDao;
import smartspace.data.ActionEntity;
import smartspace.data.ElementEntity;
import smartspace.data.ElementKey;
import smartspace.data.Location;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@PropertySource("fields.properties")
@Component
public class LikePlugin extends ActionPlugIn {

    @Autowired
    private Environment env;


    @Autowired
    public LikePlugin(nonRdbActionDao actionDao, ObjectMapper jackson, nonRdbElementDao elementDao) {
        super(actionDao, jackson, elementDao);
    }

    @Override
    public ActionEntity invoke(ActionEntity actionEntity) {
        ElementKey key = new ElementKey(actionEntity.getElementId(), actionEntity.getElementSmartSpace());
        ElementEntity product = getElementByKey(key);

        Map<String, Object> productMap = product.getMoreAttributes();
//        AtomicInteger likeCount = (AtomicInteger) productMap.get(env.getProperty("fields.product.totalLikes"));
        int likeCount = (int) productMap.get("totalLikes");

        productMap.replace("totalLikes", ++likeCount);
        productMap.replace(env.getProperty("fields.product.totalLikes"), ++likeCount);
        product.setMoreAttributes(productMap);

        elementDao.update(product);

        return actionEntity;
    }
}
